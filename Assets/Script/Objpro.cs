﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objpro : MonoBehaviour
{
    int count = 0;
    public GameObject player;
    public GameObject item;
    public GameObject coin;
    public GameObject jump;
    public GameObject blackhole;

    // Start is called before the first frame update
    void Start()
    {
       
        InvokeRepeating("Generate", 1, 1);
        int a = Random.Range(-4, 5);
        int a_ = Random.Range(-4, 5);
        int b = Random.Range(5, 9);
        int b_ = Random.Range(5, 9);
        //CreateItems();
        Instantiate(item, new Vector2(a, b), Quaternion.identity);
            Instantiate(coin, new Vector2(a_, b_), Quaternion.identity);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void Generate()
    {


        if (count < 5)
        {
            count++;
        }
        else
        {
            if (player != null)
            {
                
                float item_ = Random.Range(0, 6);
                float x = Random.Range(-4, 5);
                float y = Random.Range(Score.target.transform.position.y + 15, Score.target.transform.position.y + 35);
                
                //Vector2 position = new Vector2(x, y);
                if (item_ == 0 || item_ == 4)
                {
                    Instantiate(item, new Vector2(x, y), Quaternion.identity);
                }
                else if (item_ == 1 || item_ == 5)
                {
                    Instantiate(coin, new Vector2(x, y), Quaternion.identity);
                }
                else if (item_ == 2)
                {
                    Instantiate(jump, new Vector2(x, y), Quaternion.identity);
                }
                else if (item_ == 3)
                {
                    Instantiate(blackhole, new Vector2(x, y), Quaternion.identity);
                }
                count = 0;
            }
        }
    }

}
