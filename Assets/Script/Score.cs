﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class Score : MonoBehaviour
{
    public Text text;
    public static GameObject target;
   
    //public static float y;
    public static float max;
    
    public List<float> floatList = new List<float>();
    // Use this for initialization
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Skin");
       
    }

    // Update is called once per frame
    void Update()
    {
        

        floatList.Add(target.transform.position.y);
        if (target != null)
        {
            max = floatList.Max();
           
        }
        //Debug.Log(max);
        
        GetComponent<Text>().text = "最高地点は" + max.ToString("F0");
    }
    
}
