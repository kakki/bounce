﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyManager : MonoBehaviour
{
    static List<GameObject> dontDestroyObjects = new List<GameObject>();
    static List<GameObject> dontDestroyObjects2 = new List<GameObject>();

    static public void DontDestroyOnLoad(GameObject obj)
    {
        Object.DontDestroyOnLoad(obj);
        dontDestroyObjects.Add(obj);
    }

    static public void DestroyAll()
    {
        foreach (var obj in dontDestroyObjects)
        {
            GameObject.Destroy(obj);
        }
        dontDestroyObjects.Clear();
    }

    static public void DontDestroyOnLoad2(GameObject obj)
    {
        Object.DontDestroyOnLoad(obj);
        dontDestroyObjects2.Add(obj);
    }

    static public void DestroyAll2()
    {
        foreach (var obj in dontDestroyObjects2)
        {
            GameObject.Destroy(obj);
        }
        dontDestroyObjects2.Clear();
    }
}
