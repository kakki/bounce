﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Library : MonoBehaviour
{
    public GameObject skin0;
    public GameObject skin1;
    public GameObject skin2;
    public GameObject skin3;
    public GameObject parent;
    // Start is called before the first frame update
    void Start()
    {

        //DontDestroyManager.DontDestroyOnLoad2(gameObject);
        if (GameMgr.zero && GameMgr.zero_same)
        {
            var obj = Instantiate(skin0, new Vector2(50, -50), Quaternion.identity);
            obj.transform.SetParent(parent.transform, false);
            //GameMgr.zero_same = false;
            if (GameMgr.one && GameMgr.one_same)
            {
                var obj2 = Instantiate(skin1, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.one_same = false;
            }
            if (GameMgr.two && GameMgr.two_same)
            {
                var obj2 = Instantiate(skin2, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
            if (GameMgr.three && GameMgr.three_same)
            {
                var obj2 = Instantiate(skin3, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
        }
        else if (GameMgr.one && GameMgr.one_same)
        {
            var obj = Instantiate(skin1, new Vector2(50, -50), Quaternion.identity);
            obj.transform.SetParent(parent.transform, false);
            //GameMgr.one_same = false;
            if (GameMgr.zero && GameMgr.zero_same)
            {
                var obj2 = Instantiate(skin0, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.zero_same = false;
            }
            if (GameMgr.two && GameMgr.two_same)
            {
                var obj2 = Instantiate(skin2, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
            if (GameMgr.three && GameMgr.three_same)
            {
                var obj2 = Instantiate(skin3, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
        }
        else if (GameMgr.two && GameMgr.two_same)
        {
            var obj = Instantiate(skin2, new Vector2(50, -50), Quaternion.identity);
            obj.transform.SetParent(parent.transform, false);
            //GameMgr.two_same = false;
            if (GameMgr.zero && GameMgr.zero_same)
            {
                var obj2 = Instantiate(skin0, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.zero_same = false;

            }
            if (GameMgr.one && GameMgr.one_same)
            {
                var obj2 = Instantiate(skin1, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.one_same = false;
            }
            if (GameMgr.three && GameMgr.three_same)
            {
                var obj2 = Instantiate(skin3, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
        }
        else if (GameMgr.three && GameMgr.three_same)
        {
            var obj = Instantiate(skin3, new Vector2(50, -50), Quaternion.identity);
            obj.transform.SetParent(parent.transform, false);
            //GameMgr.two_same = false;
            if (GameMgr.zero && GameMgr.zero_same)
            {
                var obj2 = Instantiate(skin0, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.zero_same = false;

            }
            if (GameMgr.one && GameMgr.one_same)
            {
                var obj2 = Instantiate(skin1, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.one_same = false;
            }
            if (GameMgr.two && GameMgr.two_same)
            {
                var obj2 = Instantiate(skin2, new Vector2(50, -50), Quaternion.identity);
                obj2.transform.SetParent(parent.transform, false);
                //GameMgr.two_same = false;
            }
        }
    }

        // Update is called once per frame
        void Update()
        {
            //if (GameMgr.zero)
            //{
            //    Instantiate(skin0, new Vector2(50, -50), Quaternion.identity);
            //}
        }
    
}
