﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class Skin : MonoBehaviour
{
    private AudioSource se;
    private GameObject text;
    public GameObject parent;
    private Image m_Image;
    public Sprite[] m_Sprite;
    public static bool skin0= false;//ゲーム画面でのスキン変更
    public static bool skin1 = false;//ゲーム画面でのスキン変更
    public static bool skin2 = false;
    public static bool skin3 = false;
    public static bool skin4 = false;
    // Start is called before the first frame update
    void Start()
    {
        se = GetComponent<AudioSource>();
        parent = GameObject.Find("GameObject");
        text = parent.transform.Find("Change").gameObject;
        text.SetActive(false);
        m_Image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Skin0()
    {
        se.PlayOneShot(se.clip);

        skin0 = true;
        skin1 = false;
        skin2 = false;
        skin3 = false;
        skin4 = false;
        m_Image.sprite = m_Sprite[0];
        text.SetActive(true);
        Invoke("Delay", 2.0f);
       

    }
    public void Skin1()
    {
        se.PlayOneShot(se.clip);

        skin0 = false;
        skin1 = true;
        skin2 = false;
        skin3 = false;
        skin4 = false;
        m_Image.sprite = m_Sprite[1];
        text.SetActive(true);
        Invoke("Delay", 2.0f);
    }
    public void Skin2()
    {
        se.PlayOneShot(se.clip);

        skin0 = false;
        skin1 = false;
        skin2 = true;
        skin3 = false;
        skin4 = false;
        m_Image.sprite = m_Sprite[2];
        text.SetActive(true);
        Invoke("Delay", 2.0f);
    }
    public void Skin3()
    {
        se.PlayOneShot(se.clip);

        skin0 = false;
        skin1 = false;
        skin2 = false;
        skin3 = true;
        skin4 = false;
        m_Image.sprite = m_Sprite[3];
        text.SetActive(true);
        Invoke("Delay", 2.0f);
    }
    public void Origin()
    {
        se.PlayOneShot(se.clip);

        skin0 = false;
        skin1 = false;
        skin2 = false;
        skin3 = false;
        skin4 = true;
        m_Image.sprite = m_Sprite[4];
        text.SetActive(true);
        Invoke("Delay", 2.0f);
    }
    public void Delay()
    {
        text.SetActive(false);
    }
}
