﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HighScore : MonoBehaviour
{
    public Text highscoretext;
    public static float highscore;
    private string key = "HIGH SCORE";
    // Start is called before the first frame update
    void Start()
    {
        highscore = PlayerPrefs.GetFloat(key, 0);
        if (Button.reset)
        {
            highscore = 0;
            Score.max = 0;
            Button.reset = false;
        }
        else if (Score.max > highscore)
        {
            highscore = Score.max;
            PlayerPrefs.SetFloat(key, highscore);
        }
        highscoretext.text = "HIGH SCORE:" + highscore.ToString("F0");
    }

    // Update is called once per frame
    void Update()
    {
        //highscoretext.text = "HIGH SCORE:" + highscore.ToString("F0");
    }
}
