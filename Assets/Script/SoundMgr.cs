﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class SoundMgr : MonoBehaviour
{
    public AudioClip Ljump;
    public AudioClip coin;
    public AudioClip item;
    private AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Jump.jump)
        {
            audio.clip = Ljump;
            audio.PlayOneShot(audio.clip);
            Jump.jump = false;
        }
        if (Coin.coin)
        {
            audio.clip = coin;
            audio.PlayOneShot(audio.clip);
            Coin.coin = false;
        }
        if (Items.item)
        {
            audio.clip = item;
            audio.PlayOneShot(audio.clip);
           Items.item = false;
        }
    }
}
