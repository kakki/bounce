﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Ball : MonoBehaviour
{
    public GameObject ball;
    public GameObject target;
    //private Image m_Image;
    public Sprite[] m_Sprite;
    
    //Rigidbody2D rigid2d;
    //public float jumpPower = 700;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.FindGameObjectWithTag("Skin");
        
        //rigid2d = GetComponent<Rigidbody2D>();
        //m_Image = GetComponent<Image>();

        if (Skin.skin0)
        {
            ball = (GameObject)Resources.Load("Prefabs/Skin1lib");
            
            //m_Image.
           Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
            //ball.transform.SetParent(parent.transform, false);
        }
        else if (Skin.skin1)
        {
            ball = (GameObject)Resources.Load("Prefabs/Skin2lib");
            Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
            //ball.transform.SetParent(parent.transform, false);
        }
        else if (Skin.skin2)
        {
            ball = (GameObject)Resources.Load("Prefabs/Skin3lib");
            Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
            //ball.transform.SetParent(parent.transform, false);
        }

        else if(Skin.skin3)
        {
            ball = (GameObject)Resources.Load("Prefabs/Skin4lib");

            //m_Image.
            Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
        }
        else
        {
            ball = (GameObject)Resources.Load("Prefabs/origin");
            Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
        }
        //else if (Skin.skin4)
        //{
        //    ball = (GameObject)Resources.Load("Prefabs/Skin4lib");

        //    //m_Image.
        //    Instantiate(ball, new Vector2(0, 0), Quaternion.identity);
        //}
    }

    // Update is called once per frame
    void Update()
    {
      
        if (Score.target.transform.position.x > 5.58)
        {
            Score.target.transform.position = new Vector2(-5.58f, Score.target.transform.position.y);
        }
        else if (Score.target.transform.position.x < -5.58)
        {
            Score.target.transform.position = new Vector2(5.58f, Score.target.transform.position.y);
        }
        if (Score.target.transform.position.y < Score.max - 6)
        {

            //GameMgr.score = Score.y;
            SceneManager.LoadScene("GameOver");
            HighScore.highscore = Score.max;
            Destroy(gameObject);
            //Score_(score);
        }
    }
    //public void OnCollisionEnter2D(Collision2D collision)
    //{
    //    rigid2d.AddForce(transform.up * jumpPower);
    //}
}
