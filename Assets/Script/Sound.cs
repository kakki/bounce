﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class Sound : MonoBehaviour
{
    [SerializeField]
    private Slider BGMbar;
    [SerializeField]
    private Slider SEbar;
    [SerializeField]
    private Slider Masterbar;
    //private static bool sliderCheck = true;
    public AudioMixer audioMixer;
    private float volume;
    // Start is called before the first frame update
    void Start()
    {
        if (audioMixer.GetFloat("MasterVol", out volume))
        {
            Masterbar.value = volume;
            if (audioMixer.GetFloat("SEVol", out volume))
            {
                SEbar.value = volume;
                if (audioMixer.GetFloat("BGMVol", out volume))
                {
                    BGMbar.value = volume;
                    //audioMixer.SetFloat("BGMVol", -30);
                    //audioMixer.SetFloat("MasterVol", -30);
                    //audioMixer.SetFloat("SEVol", -30);
                }
            }
        }
       
        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown("4"))
        //{
        //    soundbar.SetActive(!soundbar.activeSelf);
        //}
        
    }
    public void Master(float volume)
    {
        audioMixer.SetFloat("MasterVol", volume);
    }
    public void BGM(float volume)
    {
        audioMixer.SetFloat("BGMVol", volume);
    }
    public void SE(float volume)
    {
        audioMixer.SetFloat("SEVol", volume);
    }
}

