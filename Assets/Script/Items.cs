﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class Items : MonoBehaviour
{
    public static bool item = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Skin")
        {
            MPbar.MP += 20;
            item = true;
            Destroy(gameObject);
        }
    }
}
