﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
public class Button : MonoBehaviour
{
    static public bool reset = false;

    private AudioSource se;

    // Start is called before the first frame update
    void Start()
    {
        
            se = GetComponent<AudioSource>();
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Button_Title()
    {
        se.PlayOneShot(se.clip);
        DontDestroyManager.DestroyAll();

        StartCoroutine(Delay(0.15f, "Title"));


    }
    public void Button_Game()
    {
        Score.max = 0;
        StartCoroutine(Delay(0.15f, "SampleScene"));
        se.PlayOneShot(se.clip);
    }
    public void Button_Gatya()
    {

        StartCoroutine(Delay(0.15f, "gatya"));
        se.PlayOneShot(se.clip);
    }
    public void Button_Option()
    {
        se.PlayOneShot(se.clip);
        StartCoroutine(Delay(0.15f, "option"));

    }
    public void Button_Explain()
    {
        se.PlayOneShot(se.clip);
        StartCoroutine(Delay(0.15f, "explain"));

    }
    public void Button_Reset()
    {
        se.PlayOneShot(se.clip);
        PlayerPrefs.DeleteAll();
        Debug.Log("des");
        reset = true;
        
    }
    public void Button_Library()
    {
        StartCoroutine(Delay(0.15f, "library"));
        se.PlayOneShot(se.clip);
        //DontDestroyManager.DestroyAll2();
       
    }
   IEnumerator Delay(float a,string b)
    {
        yield return new WaitForSeconds(a);
        SceneManager.LoadScene(b);
    }
}
