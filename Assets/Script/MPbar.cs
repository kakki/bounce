﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MPbar : MonoBehaviour
{
    public static bool NoMP = true;
    public GameObject Ball;
    public float time;
    public Slider sl;
    public static float MP = 100;
    // Use this for initialization
    void Start()
    {
        MP = 100;
        sl.maxValue = MP;
    }

    // Update is called once per frame
    void Update()
    {
        sl.value = MP;
        if (Input.GetMouseButton(0))
        {
            MP -= 0.25f;

        }
        if (MP <= 0)
        {
            NoMP = false;
        }
    }

}
