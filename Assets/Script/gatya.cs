﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
public class gatya : MonoBehaviour
{
    public Image s;
    public GameObject text;
    public GameObject canvas;
    public GameObject a;
    public GameObject b;
    public GameObject c;
    public GameObject d;
    public GameObject panel;
    public GameObject par;
    private AudioSource se;
    public AudioClip miss;
    int i= 0;
    // Start is called before the first frame update
    void Start()
    {
        
        s = GetComponent<Image>();
        se = GetComponent<AudioSource>();
        text.SetActive(false);
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Button()
    {
        
        Effect.b = 0;
        i = Random.Range(0,4);
        if (GameMgr.Coin >= 1)
        {
            GameMgr.Coin -= 1;
            Debug.Log("gatya");
            if (i == 1)
            {
                se.PlayOneShot(se.clip);
                Debug.Log("1");
                panel.SetActive(true);
                var obj = Instantiate(a, new Vector2(0, 15), Quaternion.identity);
                
                obj.transform.SetParent(canvas.transform, false);
                var obj2 = Instantiate(par, new Vector2(50, 15), Quaternion.identity);
                obj2.transform.SetParent(obj.transform, false);
                GameMgr.zero = true;
                StartCoroutine(Destroy(3, obj,obj2));
                Invoke("Delay_panel", 3.0f);
            }
            else if (i == 0)
            {
                se.PlayOneShot(se.clip);
                Debug.Log("0");
                panel.SetActive(true);
                var obj = Instantiate(b, new Vector2(0, 15), Quaternion.identity);
                obj.transform.SetParent(canvas.transform, false);
                var obj2 = Instantiate(par, new Vector2(50, 15), Quaternion.identity);
                obj2.transform.SetParent(obj.transform, false);
                GameMgr.one = true;
                StartCoroutine(Destroy(3, obj,obj2));
                Invoke("Delay_panel", 3.0f);
            }
            else if (i == 2)
            {
                se.PlayOneShot(se.clip);
                Debug.Log("2");
                panel.SetActive(true);
                var obj = Instantiate(c, new Vector2(0, 15), Quaternion.identity);
                obj.transform.SetParent(canvas.transform, false);
                var obj2 = Instantiate(par, new Vector2(50, 15), Quaternion.identity);
                obj2.transform.SetParent(obj.transform, false);
                GameMgr.two = true;
                StartCoroutine(Destroy(3, obj, obj2));
                Invoke("Delay_panel", 3.0f);
            }
            else if (i == 3)
            {
                se.PlayOneShot(se.clip);
                Debug.Log("3");
                panel.SetActive(true);
                var obj = Instantiate(d, new Vector2(0, 15), Quaternion.identity);
                obj.transform.SetParent(canvas.transform, false);
                var obj2 = Instantiate(par, new Vector2(50, 15), Quaternion.identity);
                obj2.transform.SetParent(obj.transform, false);
                GameMgr.three = true;
                StartCoroutine(Destroy(3, obj, obj2));
                Invoke("Delay_panel", 3.0f);
            }
        }
        else
        {
            se.PlayOneShot(miss);
            Debug.Log("コインが足りません");
            text.SetActive(true);
            Invoke("Delay_Text", 2.0f);
        }
        
    }
   
    public void Delay_Text()
    {
        text.SetActive(false);
    }
    public void Delay_panel()
    {
        panel.SetActive(false);
    }
    IEnumerator Destroy(int a, GameObject b, GameObject c)
    {
        yield return new WaitForSeconds(a);
        Destroy(b);
    }


}
