﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class Coin : MonoBehaviour
{
    public static bool coin = false;
    public static AudioSource se;
    // Start is called before the first frame update
    void Start()
    {
        se = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Skin")
        {
            
            GameMgr.Coin += 1;
            Debug.Log("coin");
            coin = true;
            Destroy(gameObject);
        }
    }
}
