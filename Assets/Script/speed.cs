﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class speed : MonoBehaviour
{
    Renderer target;
    private AudioSource se;
    Rigidbody2D rb;
    Vector2 force;
    private float maxSpeed =20;
    // Start is called before the first frame update
    void Start()
    {
       
        se = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
        force = new Vector2(0, -50);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SceneManager.LoadScene("GameOver");
        }
        if (rb.velocity.magnitude >= maxSpeed)
        {
            rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxSpeed);
            
            //rb.AddForce(force);
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "blackhole")
        {
            gameObject.SetActive(false);
            SceneManager.LoadScene("GameOver");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Line")
        {
            se.PlayOneShot(se.clip);
        }
       
    }
}
