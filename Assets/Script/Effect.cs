﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Effect : MonoBehaviour
{
    public static float b = 0;
    public float a = 0.05f;
    SpriteRenderer sp;
    bool flag = false;
    // Start is called before the first frame update
    void Start()
    {
       
        sp = GetComponent<SpriteRenderer>();
       
    }

    // Update is called once per frame
    void Update()
    {
        sp.color = new Color(1, 1, 1, b);
        b += a;
     
    }
   
    
}
